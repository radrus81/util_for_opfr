using Microsoft.AspNetCore.Http;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;

namespace Helpers
{
    public static class ExcelHelper
    {

        public static string GetSheetName(string pathFilename, int numberSheet)
        {
            using (var xlsFile = new FileStream(pathFilename, FileMode.Open, FileAccess.Read))
            {
                var hssfwb = new HSSFWorkbook(xlsFile);
                return hssfwb.GetSheetName(numberSheet);
            }
        }

        //определение приложений
        public static int? GetImportSheetIndexByExportPath(IFormFile exportFile)
        {
            using (var exportXlsFile = exportFile.OpenReadStream())
            {
                var exportXls = new HSSFWorkbook(exportXlsFile);
                string nameSheetExport = exportXls.GetSheetName(0);
                var sheetExport = exportXls.GetSheet(nameSheetExport);
                bool isFound = false;
                int? sheetActiveList = null;
                string subText = "";

                for (int exi = 0; exi < 6; exi++)
                {
                    var countCol = sheetExport.GetRow(exi).LastCellNum;
                    for (int exj = 0; exj < countCol; exj++)
                    {
                        string textFromRow = sheetExport.GetRow(exi).GetCell(exj).StringCellValue.Replace(" ", string.Empty);
                        if (!String.IsNullOrEmpty(textFromRow) && !isFound)
                        {
                            subText = textFromRow.Substring(0, 12);
                            switch (subText)
                            {
                                case "форма94(ПЕНС": sheetActiveList = 0; isFound = true; break;
                                case "Приложение№1": sheetActiveList = 1; isFound = true; break;
                                case "Приложение№2": sheetActiveList = 2; isFound = true; break;
                                case "Приложение№3": sheetActiveList = 3; isFound = true; break;
                                case "Приложение№4": sheetActiveList = 4; isFound = true; break;
                                case "Приложение№5": sheetActiveList = 5; isFound = true; break;
                                case "Приложение№6": sheetActiveList = 6; isFound = true; break;
                                case "Приложение№7": sheetActiveList = 7; isFound = true; break;
                                default:
                                    if (textFromRow == "Сведенияочисленностипенсионеров-инвалидовисуммахназначенныхимпенсий")
                                    {
                                        sheetActiveList = 9; isFound = true;
                                    }
                                    break;
                            }
                        }
                    }
                    if (isFound)
                    {
                        exportXls.Close();
                        return sheetActiveList;
                    }
                }
                exportXls.Close();
                return sheetActiveList;
            }
        }

        //копирование данных из одного файла в другой
        public static HSSFWorkbook ImportValueFromExportFile(List<(int SheetNum, IFormFile File)> activesSheetForLayout, IFormFile templateFile)
        {
            using (var importXlsFile = templateFile.OpenReadStream())
            {
                var importXls = new HSSFWorkbook(importXlsFile);

                for (int i = 0; i < activesSheetForLayout.Count; i++)
                {
                    var sheetImport = importXls.GetSheetAt(activesSheetForLayout[i].SheetNum);
                    int rowBeginImport = GetRowBegin(sheetImport);//ищем номер строки откуда нужно начать заполнять данные
                    if (rowBeginImport != sheetImport.LastRowNum)
                    {
                        //открываем файл откуда будем копировать данные
                        using (var exportXlsFile = activesSheetForLayout[i].File.OpenReadStream())
                        {
                            var exportXls = new HSSFWorkbook(exportXlsFile);
                            var sheetExport = exportXls.GetSheetAt(0);
                            int rowBeginExport = GetRowBegin(sheetExport);//ищем номер строки откуда нужно начать выбирать данные
                            if (rowBeginExport != sheetExport.LastRowNum)//если строки != № последней строки знать нашли искомое и можно начать копировать
                            {
                                CopyRows(sheetImport, rowBeginImport, sheetExport, rowBeginExport);//передаем сами объекты файлов и начало строки
                                exportXls.Close();
                            }
                            else
                            {
                                continue;
                            }
                        }
                        //RecalculationFormulas(sheetImport, rowBeginImport);//перерасчет формул с заменой на значения
                    }
                    else
                    {
                        continue;
                    }
                }

                return importXls;
            }
        }

        //перерасчет формул
        private static void RecalculationFormulas(ISheet xlsImport, int startRowImport)
        {
            int countColFromXlsImport = xlsImport.GetLastNumericCellNum(startRowImport - 1);
            int countRowsFromXlsImport = xlsImport.GetLastNumericRowNum(startRowImport);
            for (int imi = startRowImport; imi <= countRowsFromXlsImport; imi++)
            {
                for (int imj = 2; imj < countColFromXlsImport; imj++)
                {
                    ICell cellBGColor = xlsImport.GetRow(imi).GetCell(imj);

                    byte[] cellBackground = cellBGColor.CellStyle.FillForegroundColorColor.RGB;//цвет ячейки желтый [0]-255 [1]-255 [2]-153
                    string colorCell = String.Join("", cellBackground);
                    if (colorCell == "255255153")
                    {
                        ICell cell = xlsImport.GetRow(imi).GetCell(imj);
                        HSSFFormulaEvaluator fe = new HSSFFormulaEvaluator(cell.Sheet.Workbook);
                        fe.EvaluateInCell(cell);
                    }
                }
            }
        }

        //миграция данных       
        private static void CopyRows(ISheet xlsImport, int startRowImport, ISheet xlsExport, int startRowExport)
        {
            double valRowFromXlsExport = 0;
            int countColFromXlsImport = xlsImport.GetLastNumericCellNum(startRowImport - 1);
            int countRowsFromXlsImport = xlsImport.GetLastNumericRowNum(startRowImport);

            for (int imi = startRowImport; imi <= countRowsFromXlsImport; imi++)
            {
                double? numColFromXlsImport, numColFromXlsExport;
                numColFromXlsImport = GetNumberFromRow(xlsImport, imi);//получаем номер строки из файла шаблона
                numColFromXlsExport = GetNumberFromRow(xlsExport, startRowExport);////получаем номер строки из файла экспорта

                if (numColFromXlsImport == null || numColFromXlsExport == null)
                {
                    if (numColFromXlsExport == null)//если в столбце нумер. строк файла экспорта пустота
                    {
                        startRowExport++;
                    }
                    if (numColFromXlsImport == null)//если в столбце нумер. строк файла импорта пустота
                    {
                        continue;
                    }
                }
                //тут ситуация когда в файле экспорта идет доп. строка с нумерацией колонок
                if (numColFromXlsImport > numColFromXlsExport)
                {
                    startRowExport++;//перепрыгиваем эту строку                    
                }

                for (int imj = 2; imj < countColFromXlsImport; imj++)
                {

                    CellType typeCell = xlsExport.GetRow(startRowExport).GetCell(imj).CellType;
                    ICell cellBGColor = xlsImport.GetRow(imi).GetCell(imj);

                    byte[] cellBackground = cellBGColor.CellStyle.FillForegroundColorColor.RGB;//цвет ячейки желтый [0]-255 [1]-255 [2]-153
                    string colorCell = String.Join("", cellBackground);
                    if (CellType.Numeric == typeCell)
                    {
                        valRowFromXlsExport = xlsExport.GetRow(startRowExport).GetCell(imj).NumericCellValue;
                        WriteText(numColFromXlsImport + "==" + numColFromXlsExport + "Row=" + imi.ToString() + ";Col= " + imj + ";" + ";val= " + valRowFromXlsExport + "\n");//это для отладки, нужно было посмотреть на какой тормозит
                    }
                    if (colorCell == "000") //проверить цвет ячейки и если 000...
                    {
                        xlsImport.GetRow(imi).GetCell(imj).SetCellValue(valRowFromXlsExport);//копируем данне                        
                    }
                }
                startRowExport++;
            }
        }

        //получение номера последней колонки
        private static int GetLastNumericCellNum(this ISheet xlsImport, int startRowImport)
        {
            int CountCol = xlsImport.GetRow(startRowImport).LastCellNum;
            CellType typeCell;
            for (int i = 0; i < CountCol; i++)
            {
                typeCell = xlsImport.GetRow(startRowImport).GetCell(i).CellType;
                if (CellType.Numeric != typeCell)
                {
                    CountCol = i;
                    break;
                }
            }
            return CountCol;
        }

        //получение последнего номера строки
        private static int GetLastNumericRowNum(this ISheet xlsImport, int startRowImport)
        {
            int CountRows = xlsImport.LastRowNum;
            CellType typeCell;
            for (int i = startRowImport; i < CountRows; i++)
            {
                try
                {
                    typeCell = xlsImport.GetRow(i).GetCell(0).CellType;
                }
                catch
                {
                    CountRows = i - 1;
                    break;
                }
            }
            return CountRows;
        }

        //получение значения из колонки с нумерацией строки
        private static double? GetNumberFromRow(ISheet xlsFile, int rowNum)
        {
            var cellType = xlsFile.GetRow(rowNum).GetCell(1).CellType;
            switch (cellType)
            {
                case CellType.Numeric:
                case CellType.Formula:
                    return xlsFile.GetRow(rowNum).GetCell(1).NumericCellValue;
                case CellType.String:
                    var stringValue = xlsFile.GetRow(rowNum).GetCell(1).StringCellValue.Replace(" ", string.Empty);
                    if (double.TryParse(stringValue, out var numFromRow))
                    {
                        return numFromRow;
                    }
                    else
                    {
                        return null;
                    }
                default:
                    return null;
            }
        }

        //поиск строки откуда нужно начать заполнять данными ячейки
        private static int GetRowBegin(ISheet xls)
        {
            const int COLUMN_NUMBER_PARAMETERS_NAME = 1;
            int i;
            double valueRowExport = 0;
            for (i = 5; i < xls.LastRowNum; i++)
            {

                var typeCell = xls.GetRow(i).GetCell(1).CellType;
                switch (typeCell)
                {
                    case CellType.Numeric:
                    case CellType.Blank:
                        valueRowExport = xls.GetRow(i).GetCell(1).NumericCellValue;//значение из колонки с нумерацией строк
                        break;
                    case CellType.String:

                        if (!double.TryParse(xls.GetRow(i).GetCell(1).StringCellValue, out valueRowExport))//значение из колонки с нумерацией строк
                        {
                            continue;
                        }
                        break;

                }
                if (valueRowExport == COLUMN_NUMBER_PARAMETERS_NAME)
                {
                    break;
                }
            }
            return i;
        }

        //запись в файл, логирование
        static void WriteText(string args)
        {
            File.AppendAllText("D:\\test.txt", args); //допишет текст в конец файла
        }
    }
}