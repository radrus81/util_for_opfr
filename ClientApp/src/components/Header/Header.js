import React from 'react'
import './Header.scss'

//import logo from '../../img/icon/logo.png'

const Header = () => {
    return (
        <header>
            <div className="header wrap">
                <h1>Копир Inso_buh. Версия 3.0</h1>
            </div>
        </header>
    )
}

export default Header 