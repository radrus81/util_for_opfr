import React from 'react'
import './Footer.scss'

const Footer = () => {
    return (
        <footer>
            <div className="footer wrap">
                <h3>ОПФР по Ульяновской области 2019 год.</h3>
            </div>
        </footer>
    )
}

export default Footer 