import React, { Component, Fragment } from 'react'
import './Main.scss'
import FeedbackFormSchema from './FeedbackFormSchema'
import { Formik } from 'formik'
import axios from 'axios'


const FormState = {
    Initial: '',
    Submitting: 'submitting',
    Error: 'error',
    Success: 'success'
}

class Main extends Component {

    constructor(props) {
        super(props)
        this.state = {
            FormState: FormState.Initial,
            TextError: '',
            BlobXls: null,
            ResponseHeaders: null
        }
    }

    clearMessage = () => {
        if (this.state.FormState !== FormState.Initial || this.state.FormState !== FormState.Submitting) {
            this.setState({ FormState: FormState.Initial })
        }
    }

    createFileXls = () => {
        const url = this.state.BlobXls
        const link = document.createElement('a');
        link.href = url;
        const contentDisposition = this.state.ResponseHeaders['content-disposition'];
        let fileName = 'unknown';
        if (contentDisposition) {
            const fileNameMatch = contentDisposition.match('filename=(.+);');
            if (fileNameMatch.length === 2)
                fileName = fileNameMatch[1];
        }
        link.setAttribute('download', fileName);
        document.body.appendChild(link);
        link.click();
    }

    render() {
        return (
            <section>
                <div className="main wrap">
                    <Formik
                        initialValues={{
                            Layout: null,
                            ImportFile: null
                        }}
                        validationSchema={FeedbackFormSchema}
                        onSubmit={async (values, { setSubmitting }) => {

                            var data = new FormData();
                            data.append('TemplateFile', values.Layout)
                            for (let i = 0; i < values.ImportFile.length; i++) {
                                data.append('ImportFiles', values.ImportFile[i])
                            }

                            axios.interceptors.request.use(config => {
                                this.setState({ FormState: FormState.Submitting })
                                return config;
                            }, function (error) {
                                this.setState({ FormState: FormState.Error })
                                return Promise.reject(error);
                            })

                            axios({
                                url: 'api/InsoBuh',
                                method: 'POST',
                                responseType: 'blob',
                                data: data
                            })
                                .then(response => {
                                    this.setState({
                                        FormState: FormState.Success,
                                        BlobXls: window.URL.createObjectURL(new Blob([response.data])),
                                        ResponseHeaders: response.headers
                                    })
                                    this.createFileXls()
                                    setSubmitting(false)
                                })
                                .catch(error => {
                                    this.setState({ FormState: FormState.Error, TextError: error.message })
                                })
                            setTimeout(() => {
                                this.clearMessage()
                            }, 150000);

                            setSubmitting(false)
                        }}


                        render={({ errors, touched, isSubmitting, handleSubmit, setFieldValue }) => (
                            <Fragment>
                                <form onSubmit={handleSubmit}>
                                    {
                                        errors.Layout && touched.Layout &&
                                        <div className="alert alert-danger alert-dismissible show">{errors.Layout}</div>
                                    }
                                    <div className="form-group">
                                        <label htmlFor="Layout">Выберите файл шаблона, куда нужно загрузить данные:</label>
                                        <input
                                            name="Layout"
                                            type="file"
                                            className="form-control"
                                            onChange={(event) => {
                                                setFieldValue("Layout", event.currentTarget.files[0])
                                            }}
                                        />
                                    </div>
                                    {
                                        errors.ImportFile &&
                                        touched.ImportFile && <div className="alert alert-danger alert-dismissible show">{errors.ImportFile} </div>

                                    }
                                    <div className="form-group">
                                        <label htmlFor="ImportFile">Выберите файлы выгуженные из ПТК НВП:</label>
                                        <input
                                            name="ImportFile"
                                            type="file"
                                            className="form-control"
                                            onChange={(event) => {
                                                setFieldValue("ImportFile", event.currentTarget.files)
                                            }}
                                            multiple />
                                    </div>
                                    <button id="send" type="submit" className="btn btn-primary" disabled={isSubmitting} >Скопировать</button>
                                </form>
                                {this.state.FormState === FormState.Submitting ?

                                    <div className="alert alert-warning" role="alert">
                                        <strong>Ждите! Идет обработка файлов....</strong>
                                    </div>
                                    : null
                                }
                                {this.state.FormState === FormState.Success ?
                                    <Fragment>
                                        <div className="alert alert-success" role="alert">
                                            <strong>Успешно!</strong>
                                        </div>
                                        <button id="downXls" type="button" className="btn btn-success" onClick={this.createFileXls}>Скачать файл</button>
                                    </Fragment>
                                    : null
                                }
                                {this.state.FormState === FormState.Error ?
                                    <div className="alert alert-danger" role="alert">
                                        <strong>Произошла ошибка! <p>{this.state.TextError}</p></strong>

                                    </div>
                                    : null
                                }
                            </Fragment>
                        )}
                    />
                </div>
            </section >
        )
    }
}

export default Main 