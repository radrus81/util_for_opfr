import * as Yup from "yup";

const FeedbackFormSchema = Yup.object().shape({
    Layout: Yup.string().nullable()
        .required("Необходимо выбрать файл шаблона."),
    ImportFile: Yup.string().nullable()
        .required("Необходимо выбрать файлы для импорта.")

});

export default FeedbackFormSchema