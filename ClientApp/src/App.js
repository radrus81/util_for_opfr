import React, { Component } from 'react';
import Header from './components/Header/Header'
import Main from './components/Section/Main'
import Footer from './components/Footer/Footer'

export default class App extends Component {

  render() {
    return (
      <div>
        <Header />
        <Main />
        <Footer />
      </div>
    );
  }
}
