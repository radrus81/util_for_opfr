using Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;


namespace InsoBuh.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class InsoBuhController : Controller
    {

        [HttpPost]
        public IActionResult FillTemplateFilesInsoBuh(IFormFile templateFile, List<IFormFile> ImportFiles)
        {
            //кортеж
            var activesSheetForLayout = new List<(int SheetNum, IFormFile File)>();

            foreach (var file in ImportFiles)
            {
                int? activeSheetForLayout = ExcelHelper.GetImportSheetIndexByExportPath(file);//определили на какую страницу нужно грузить данные

                if (activeSheetForLayout != null)
                {
                    //грузим в список номер листа и путь к файлу
                    activesSheetForLayout.Add((activeSheetForLayout.Value, file));
                }

            }

            var modifiedTemplate = ExcelHelper.ImportValueFromExportFile(activesSheetForLayout, templateFile);//передаем полученный массив для заполнения + путь к шаблону

            var memory = new MemoryStream();
            modifiedTemplate.Write(memory);
            memory.Position = 0;
            return File(memory, templateFile.ContentType, templateFile.FileName);

        }

    }
}
